﻿// Praktika C++.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//


#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <string.h>
#include <cstdlib>
#include <algorithm>
#include <fstream>
#include <vector>
#include <list>

using namespace std;

class Apartment {

	string Adres;
	int NumerHaus;
	int NumerApartment;
	int CountRoom;
	int SquareMetr;
	string KadasterNum;
	int RegNumer;
	string Status;


public:

	Apartment() {

		Adres = " ";
		NumerHaus = 0;
		NumerApartment = 0;
		CountRoom = 0;
		SquareMetr = 0;
		KadasterNum = " ";
		RegNumer = 0;
		Status = " ";

	}
	void Enter(string iAdres, int iNumerHaus, int iNumerApartment, int iCountRoom, int iSquareMetr, string iKadasterNum, int iRegNumer, string iStatus) {

		Adres = iAdres;
		NumerHaus = iNumerHaus;
		NumerApartment = iNumerApartment;
		CountRoom = iCountRoom;
		SquareMetr = iSquareMetr;
		KadasterNum = iKadasterNum;
		RegNumer = iRegNumer;
		Status = iStatus; 
	}


	void Print() {

		cout << Adres << ",\t" <<"Haus № "<< NumerHaus << ",\tRoom № " << NumerApartment 
			<< ",  CountRoom - " << CountRoom << ", " << SquareMetr << " - m2,\tKadasterNum - " << KadasterNum << ",\tRegNumer - "<<RegNumer<<",\tStatus - "<< Status << endl;
	}

	int GetCountRoom() {

		return CountRoom;
	}
	int GetSquareMetr() {

		return SquareMetr;

	}
	int GetRegNumer() {

		return RegNumer;
	}
	string GetStatus(){

		return Status;
	}
	string SetStatus(string NewStatus) {
		Status = NewStatus;
		return Status;

	}

};
class Rieltor {

	Apartment* mass;
	string Name;
	int size;


public:
	Rieltor() {
		mass = NULL;
		Name = " ";
		size = 0;
	}
	Rieltor(string iName) :Name{ iName } {

	}
	void AddApartment(string iAdres, int iNumerHaus, int iNumerApartment, int iCountRoom, int iSquareMetr, string iKadasterNum, int iRegNumer, string iStatus) {

		Apartment* NewMass = new Apartment[++size];

		for (int i = 0; i < size - 1; i++)
		{
			NewMass[i] = mass[i];
		}

		NewMass[size - 1].Enter(iAdres, iNumerHaus, iNumerApartment, iCountRoom, iSquareMetr, iKadasterNum, iRegNumer, iStatus);
		delete[] mass;
		mass = NewMass;
	}
	void Print() {

		cout << "----------------------------------------" << endl;

		cout << Name << "\t" << endl;

		for (int i = 0; i < size; i++)
		{
			mass[i].Print();
		}cout << endl;
	}

	void PrintCountRoom(int r) {

		cout << "----------------------------------------" << endl;

		cout << Name << "\t" << endl;

		cout << " Сортировка по количеству комнат!" << endl;

		for (int i = 0; i < size; i++)
		{
			if (mass[i].GetCountRoom() == r)
			{
				mass[i].Print();
			}
		}
	}
	void PrintSquareMetr(int m) {

		cout << "----------------------------------------" << endl;

		cout << Name << "\t" << endl;

		cout << " Сортировка по количеству квадратных метров!" << endl;

		for (int i = 0; i < size; i++)
		{
			if (mass[i].GetSquareMetr() == m)
			{
				mass[i].Print();
			}
		}
	}

	void PrintRegNumer(int n) {

		cout << "----------------------------------------" << endl;

		cout << Name << "\t" << endl;

		cout << " Сортировка по регистрационному номеру!" << endl;

		for (int i = 0; i < size; i++)
		{
			if (mass[i].GetRegNumer() == n)
			{
				mass[i].Print();
			}
		}
	}

	void ChangStatus(int RNumer, string nStatus) {

		for (int i = 0; i < size; i++)
		{
			if (mass[i].GetRegNumer() == RNumer)
			{
				mass[i].SetStatus(nStatus);
			}
		}
	}

};

int main()
{

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	//srand(time(NULL));

	Apartment* mass[20];



	Rieltor  A1("Etazhi:");
	Rieltor  A2("Koshelev:");



	A1.AddApartment("Kaluga, Kirova street", 58, 25, 4, 90, "dfsdg32135645", 155, "Registered");
	A1.AddApartment("Kaluga, Kirova street", 48, 15, 3, 70, "dqewg32146564", 156, "Registered");
	A1.AddApartment("Kaluga, Kirova street", 12, 5, 2, 45, "dopuig32134685", 157, "Registered");
	A1.AddApartment("Kaluga, Lenina street", 18, 35, 2, 40, "dfsdg321sdg84", 158, "Registered");
	A1.AddApartment("Kaluga, Lenina street", 6, 3, 4, 90, "dqewg3214656491", 159, "Registered");
	A1.AddApartment("Kaluga, Zhukova street", 8, 16, 2, 45, "dopuig32135f5", 160, "Registered");
	A1.AddApartment("Kaluga, Kirova street", 48, 15, 3, 70, "dfsdg32dgd864", 161, "Registered");

	A2.AddApartment("Kaluga, Gagarina street", 58, 25, 4, 90, "dfsdg3214564", 222, "Registered");
	A2.AddApartment("Kaluga, Volkova street", 48, 15, 3, 70, "dqewg32146554", 223, "Registered");
	A2.AddApartment("Kaluga, Mayakov street", 12, 5, 2, 45, "dopuig32138564", 224, "Registered");
	A2.AddApartment("Kaluga, Kirova street", 63, 15, 1, 35, "dfsdg346134sg6", 225, "Registered");
	A2.AddApartment("Kaluga, Lenina street", 5, 12, 3, 60, "dfsdg32fg5hfdg4", 226, "Registered");
	A2.AddApartment("Kaluga, Lenina street", 6, 31, 4, 90, "dqewg3256226564", 227, "Registered");
	A2.AddApartment("Kaluga, Zhukova street", 28, 16, 2, 45, "dopuig5fgf574", 228, "Registered");
	A2.AddApartment("Kaluga, Kirova street", 48, 15, 3, 70, "dfsdg32dgd8634", 229, "Registered");


	A1.Print();

	cout << "----------------------------------------" << endl;

	A2.Print();

	cout << "----------------------------------------" << endl;

	A1.PrintCountRoom(3);


	cout << "----------------------------------------" << endl;

	A2.PrintSquareMetr(45);

	cout << "----------------------------------------" << endl;

	A1.PrintRegNumer(155);

	cout << "----------------------------------------" << endl;

	A1.ChangStatus(157, " Sold");
	A1.ChangStatus(155, " Sold");
	A1.ChangStatus(159, " Sold");

	A1.Print();


	return 0;
}




//*****************************************************
//class Pacient
/*
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <string.h>
#include <cstdlib>
#include <algorithm>
#include <fstream>
#include <vector>
#include <list>

using namespace std;



class Pacient {
	string FIO;
	int Age;
	string Diagnos;

public:

	Pacient() {

		FIO = " ";
		Age = 42;
	}


	void Enter(string iFIO, int iAge) {

		FIO = iFIO;
		Age = iAge;
	}

	void AddDiagnos(string iDiagnos) {

		Diagnos = iDiagnos;
	}

	void Print() {

		cout << FIO << '\t' << Age << '\t' << Diagnos << endl;
	}


};
class Poliklinika {

	Pacient* mass;
	int size;
	string Name;


public:

	Poliklinika() {
		mass = NULL;
		size = 0;
		Name = " ";

	}

	Poliklinika(string Name) {
		mass = NULL;
		size = 0;
		Name = " ";

	}


	void AddPacient(string iFIO, int iAge) {

		Pacient* NewMass = new Pacient[++size];

		for (int i = 0; i < size - 1; i++)
		{
			NewMass[i] = mass[i];
		}

		NewMass[size - 1].Enter(iFIO, iAge);
		delete[] mass;
		mass = NewMass;


	}
	void Print() {

		cout << "----------------------------------------" << endl;

		cout << Name << "\t" << endl;

		for (int i = 0; i < size; i++)
		{
			mass[i].Print();
		}cout << endl;


	}


};


int main()
{

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	//srand(time(NULL));


	Pacient* mass = new Pacient[5];

	Poliklinika P1("Pirogova");

	P1.AddPacient("1 - Petrov", 42);
	P1.AddPacient("2 - Sidorov", 37);
	P1.AddPacient("3 - Shuvalov", 54);
	P1.AddPacient("4 - Demidov", 35);
	P1.AddPacient("5 - Ivanova", 28);
	P1.AddPacient("6 - Filimonova", 25);


	P1.Print();



	string dig;

	cout << "----------------------------------------" << endl;

	for (int i = 0; i < size; i++)
	{
		cout << "Введите диагноз пациента!" << "№ " << i + 1 << endl;
		cin >> dig;
		mass[i].AddDiagnos(dig);
	}

	cout << "----------------------------------------" << endl;





	return 0;
}

*/

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
